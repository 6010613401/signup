class SessionsController < ApplicationController
    skip_before_action :authorize
  def new
  end

  def create
      user = User.find_by(username: params[:username])
      if user and user.authenticate(params[:password])
          session[:user_id] = user_id
          session[:user_username] = user.username
          redirect_to admin_url, alert: "User logged in"
      else
          redirect_to login_url, alert:"Invalid Username or Password"
      end
  end

  def destroy
      session[:user_id] = nil
      redirect_to login_url, alert:"Successfully logged out"
  end
end